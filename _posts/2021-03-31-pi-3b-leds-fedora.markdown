---
layout: post
title:  "Turning off the RPi3B power LED in Fedora"
date:   2021-03-31 11:42:50 +0100
categories: [fedora, rpi, leds]
---

# The problem
Fedora doesn't support the sysfs GPIO interface, and since it isn't running the official raspbian kernel, you can't use the GPU driver as listed on the RPi forums.

# What I tried first
At first I tried enabling the configuration options in `/boot/efi/config.txt`, but this had no effect, presumably due to the switch of the LEDs to the GPIO expander.

# Enabling GPIO
Since the LED is on a GPIO extender, I wondered if Fedora would be able to see it via the GPIO. First follow https://fedoraproject.org/wiki/Architectures/ARM/gpio#Using_the_GPIO to install libgpiod, and run `sudo gpioinfo`. At the end you should see this output:
```
gpiochip1 - 8 lines:
	line   0:      "BT_ON"   "shutdown"  output  active-high [used]
	line   1:      "WL_ON"      "reset"  output   active-low [used]
	line   2: "STATUS_LED"        "ACT"  output  active-high [used]
	line   3:    "LAN_RUN"       unused  output  active-high 
	line   4: "HDMI_HPD_N"       unused   input  active-high 
	line   5:  "CAM_GPIO0"       unused  output  active-high 
	line   6:  "CAM_GPIO1"       unused  output  active-high 
	line   7:  "PWR_LOW_N"       unused   input  active-high 
```
Success! On line 7 you see the power LED.

# Turning off the LED
It's as simple as setting that pin to 1 - `sudo gpioset gpiochip1 7=1` will turn off the LED.

