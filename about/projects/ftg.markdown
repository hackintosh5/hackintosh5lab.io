---
layout: page
title: Friendly-Telegram
permalink: /about/projects/ftg/
---

Friendly-Telegram is a modular and secure userbot for Telegram, written in Python.

The documentation is [here](https://friendly-telegram.gitlab.io) and the source code is [here](https://gitlab.com/hackintosh5/friendly-telegram).
