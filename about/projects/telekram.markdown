---
layout: page
title: Telekram
permalink: /about/projects/telekram/
---

Telekram is a Kotlin MTProto library. It provides a backend usable by Telegram clients to communicate with the Telegram servers. It is incomplete - if you want to take over, [email me](blog@hack5.dev)!
