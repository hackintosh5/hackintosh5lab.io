---
layout: page
title: Treble Info
permalink: /about/projects/TrebleInfo/
---

Treble Info provides information about the Project Treble system type. It determines the specific type of GSI that needs to be installed for a given phone. The source code is available [here](https://gitlab.com/TrebleInfo/TrebleInfo) and it can be downloaded from [Google Play](https://play.google.com/store/apps/details?id=tk.hack5.treblecheck) or [GitLab](https://gitlab.com/TrebleInfo/TrebleInfo/-/releases).
